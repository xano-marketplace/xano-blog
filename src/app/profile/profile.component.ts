import {Component, OnInit} from '@angular/core';
import {AuthService} from '../auth.service';
import {MatDialog} from '@angular/material/dialog';
import {ManagePanelComponent} from '../manage-panel/manage-panel.component';
import {MatSnackBar} from '@angular/material/snack-bar';
import {debounceTime, distinctUntilChanged, finalize} from 'rxjs/operators';
import {FormControl} from '@angular/forms';

@Component({
	selector: 'app-my-blogs',
	templateUrl: './profile.component.html',
	styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
	public blogs: any[] = [];
	public name: string;
	public currentPage: number;
	public loading: boolean = true;
	public totalItems: number;
	public search: FormControl = new FormControl('');

	constructor(
		private authService: AuthService,
		private dialog: MatDialog,
		private snackBar: MatSnackBar,
	) {
	}

	ngOnInit(): void {
		this.getProfile(null);

		this.search.valueChanges
			.pipe(
				debounceTime(300),
				distinctUntilChanged(),
			)
			.subscribe((value) => {
				if (value.length > 0) {
					value = value.trim();
					const search = {
						expression: [{
							type: 'group',
							group: {
								expression: [
									{
										statement: {
											left: {
												tag: 'col',
												operand: 'blog.title'
											},
											op: 'includes',
											right: {
												operand: `${value}`
											}
										}
									},
									{
										or: true,
										statement: {
											left: {
												tag: 'col',
												operand: 'blog.description'
											},
											op: 'includes',
											right: {
												operand: `${value}`
											}
										}
									},
									{
										or: true,
										statement: {
											left: {
												tag: 'col',
												operand: 'blog.content'
											},
											op: 'includes',
											right: {
												operand: `${value}`
											}
										}
									},
									{
										'or': true,
										'statement': {
											'left': {
												'tag': 'col',
												'operand': 'blog.tags'
											},
											'op': 'includes',
											'right': {
												'operand': `${value}`
											}
										}
									}
								]
							}
						}]
					};
					this.getProfile(search);
				} else {
					this.getProfile(null);
				}
			});
	}

	public openManagePanel(blog): void {
		const dialogRef = this.dialog.open(ManagePanelComponent, {data: {blog}, minWidth: '50vw'});

		dialogRef.afterClosed().subscribe(res => {
			if (res?.deleted) {
				this.snackBar.open('Blog', 'Deleted');
				this.blogs = this.blogs.filter(x => x.id !== res.item.id);
			} else if (res?.updated) {
				this.blogs[this.blogs.findIndex(x => x.id === res.item.id)] = res.item;
				this.snackBar.open('Blog', 'Updated');
			} else if (res?.new) {
				this.blogs.unshift(res.item);
			}
		});
	}

	private getProfile(search) {
		this.loading = true;
		this.authService.profile(search)
			.pipe(
				finalize(() => this.loading = false)
			)
			.subscribe(res => {
				this.name = res.user.name;
				this.currentPage = res.curPage;
				this.totalItems = res.itemsTotal;
				this.blogs = res.items;
			});
	}

	public pageChangeEvent(event): void {
		this.getProfile({page: event.pageIndex + 1});
	}

	public clearSearch() {
		this.getProfile(null);
		this.search.patchValue('');
	}
}
