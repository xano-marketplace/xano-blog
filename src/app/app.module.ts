import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {HomeComponent} from './home/home.component';
import {HttpClientModule} from '@angular/common/http';
import {ExploreComponent} from './explore/explore.component';
import {ReactiveFormsModule} from '@angular/forms';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {QuillModule} from 'ngx-quill';
import {ProfileComponent} from './profile/profile.component';
import {CommentsComponent} from './comments/comments.component';
import {ActionBarComponent} from './action-bar/action-bar.component';
import {MatButtonModule} from '@angular/material/button';
import {MatIconModule} from '@angular/material/icon';
import {MAT_DIALOG_DEFAULT_OPTIONS, MatDialogModule} from '@angular/material/dialog';
import {ConfigPanelComponent} from './config-panel/config-panel.component';
import {MatInputModule} from '@angular/material/input';
import {AuthPanelComponent} from './auth-panel/auth-panel.component';
import {MAT_SNACK_BAR_DEFAULT_OPTIONS, MatSnackBarModule} from '@angular/material/snack-bar';
import {ManagePanelComponent} from './manage-panel/manage-panel.component';
import {MatChipsModule} from '@angular/material/chips';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {MatButtonToggleModule} from '@angular/material/button-toggle';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {MatCardModule} from '@angular/material/card';
import {HeaderComponent} from './header/header.component';
import {SafeHtmlPipe} from './safe-html.pipe';
import {MatPaginatorModule} from '@angular/material/paginator';
import {BlogViewComponent} from './blog-view/blog-view.component';

@NgModule({
	declarations: [
		AppComponent,
		BlogViewComponent,
		HomeComponent,
		HeaderComponent,
		ExploreComponent,
		ProfileComponent,
		CommentsComponent,
		ActionBarComponent,
		ConfigPanelComponent,
		AuthPanelComponent,
		ManagePanelComponent,
		SafeHtmlPipe,
	],
	imports: [
		BrowserModule,
		BrowserAnimationsModule,
		AppRoutingModule,
		HttpClientModule,
		QuillModule.forRoot(),
		ReactiveFormsModule,
		MatButtonModule,
		MatIconModule,
		MatDialogModule,
		MatSnackBarModule,
		MatInputModule,
		MatChipsModule,
		MatAutocompleteModule,
		MatButtonToggleModule,
		MatProgressSpinnerModule,
		MatCardModule,
		MatPaginatorModule
	],
	providers: [{
		provide: MAT_SNACK_BAR_DEFAULT_OPTIONS,
		useValue: {
			duration: 4000,
			horizontalPosition: 'start'
		}
	}, {
		provide: MAT_DIALOG_DEFAULT_OPTIONS,
		useValue: {
			hasBackdrop: true,
			minHeight: '100vh',
			position: {top: '0px', right: '0px'},
			panelClass: 'slide-out-panel'
		}
	}
	],
	bootstrap: [AppComponent]
})
export class AppModule {
}
