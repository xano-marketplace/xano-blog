import {AfterViewInit, ChangeDetectorRef, Component, EventEmitter, OnInit} from '@angular/core';
import {BlogService} from '../blog.service';
import {debounceTime, distinctUntilChanged, finalize, tap} from 'rxjs/operators';
import * as _ from 'lodash-es';
import {ConfigService} from '../config.service';
import {FormControl} from '@angular/forms';
import {MatSnackBar} from '@angular/material/snack-bar';
import {ActivatedRoute} from '@angular/router';
import {get} from 'lodash-es';

export interface Category {
	id: number;
	category: string;
	blog_top3: any;
}

@Component({
	selector: 'app-explore',
	templateUrl: './explore.component.html',
	styleUrls: ['./explore.component.scss']
})
export class ExploreComponent implements OnInit {
	public blogs: any[] = [];
	public form: any;
	public search: FormControl = new FormControl('');
	public loading: boolean = true;
	public categories: Category[];
	public loggedIn: boolean = false;
	public filterCategoryName: string | null = null;
	public filterCategoryID: number | null = null;
	public currentPage: number;
	public totalItems: number;

	constructor(
		private blogService: BlogService,
		private Service: BlogService,
		private configService: ConfigService,
		private snackBar: MatSnackBar,
		private route: ActivatedRoute
	) {
	}

	ngOnInit(): void {
		this.configService.isLoggedIn().subscribe(loggedIn => this.loggedIn = loggedIn);

		this.blogService.categoryGet().toPromise().then(categories => {
			this.categories = categories;
		});

		this.search.valueChanges
			.pipe(
				debounceTime(300),
				distinctUntilChanged(),
			)
			.subscribe((value) => {
				if (value.length) {
					this.getBlogs();
				}
			});

		const filterByTag = this.route.snapshot.params.tag;

		if (filterByTag) {
			this.search.patchValue(filterByTag);
		}
	}

	public getBlogs(categoryId = null, page = 1): void {
		const search = {
			page: page,
			expression: []
		};

		const searchValue = this.search.value.trim();

		if (searchValue) {
			search.expression.push({
				'type': 'group',
				'group': {
					'expression': [
						{
							'statement': {
								'left': {
									'tag': 'col',
									'operand': 'blog.title'
								},
								'op': 'includes',
								'right': {
									'operand': `${searchValue}`
								}
							}
						},
						{
							'or': true,
							'statement': {
								'left': {
									'tag': 'col',
									'operand': 'blog.description'
								},
								'op': 'includes',
								'right': {
									'operand': `${searchValue}`
								}
							}
						},
						{
							'or': true,
							'statement': {
								'left': {
									'tag': 'col',
									'operand': 'blog.content'
								},
								'op': 'includes',
								'right': {
									'operand': `${searchValue}`
								}
							}
						},
						{
							'or': true,
							'statement': {
								'left': {
									'tag': 'col',
									'operand': 'blog.tags'
								},
								'op': 'includes',
								'right': {
									'operand': `${searchValue}`
								}
							}
						}
					]
				}
			});
		}

		this.blogService.blogsGet(search, categoryId).pipe(
			tap(() => this.loading = true),
			finalize(() => this.loading = false)
		).subscribe(res => {
			if(res && get(res, 'items', false)) {
				this.blogs = res.items;
				this.totalItems = res.itemsTotal;
				this.currentPage = res.curPage;
			}
		}, (err) => {
			this.snackBar.open(_.get(err, 'error.message', 'An unknown error has occurred.'), 'Error', {panelClass: 'error-snack'});
		});
	}

	public clearSearch() {
		this.blogs = [];
		this.search.patchValue('');
		this.filterCategoryName = null;
		this.filterCategoryID = null;
	}

	public filterByCategory(category) {
		this.filterCategoryName = category.category;
		this.filterCategoryID = category.id;
		this.getBlogs(category.id);
	}

	public pageChangeEvent(event): void {
		this.getBlogs(this.filterCategoryID, event.pageIndex + 1);
	}
}
