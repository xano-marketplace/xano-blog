import {Component, ElementRef, Inject, OnInit, ViewChild} from '@angular/core';
import {ConfigService, XanoConfig} from '../config.service';
import {BlogService} from '../blog.service';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {COMMA, ENTER} from '@angular/cdk/keycodes';
import {MatChipInputEvent} from '@angular/material/chips';
import {MatAutocomplete, MatAutocompleteSelectedEvent} from '@angular/material/autocomplete';
import {Observable} from 'rxjs';
import {finalize, map, startWith, tap} from 'rxjs/operators';
import {MatSnackBar} from '@angular/material/snack-bar';
import {get} from 'lodash-es';

@Component({
	selector: 'app-manage-panel',
	templateUrl: './manage-panel.component.html',
	styleUrls: ['./manage-panel.component.scss']
})
export class ManagePanelComponent implements OnInit {
	public config: XanoConfig;
	readonly separatorKeysCodes: number[] = [ENTER, COMMA];
	public deleting: boolean = false;
	public saving: boolean = false;
	public uploading: boolean = false;
	public image: any;

	public blogForm: FormGroup = new FormGroup({
		id: new FormControl(null),
		title: new FormControl('', Validators.required),
		description: new FormControl('', Validators.required),
		content: new FormControl('', Validators.required),
		slug: new FormControl('', Validators.required),
		published: new FormControl(true),
		published_at: new FormControl(null),
		user_id: new FormControl(null, Validators.required),
		banner: new FormControl(null)
	});

	public tags = new Set();
	public categories = new Set();
	public availableCategories = [];
	public categoryControl: FormControl = new FormControl('');
	public filteredCategories: Observable<string[]>;
	@ViewChild('categoryInput') categoryInput: ElementRef<HTMLInputElement>;
	@ViewChild('autocomplete') matAutocomplete: MatAutocomplete;

	public modules = {
		toolbar: [
			['bold', 'italic', 'underline'],
			['blockquote'],
			[{header: 1}, {header: 2}],
			[{list: 'ordered'}, {list: 'bullet'}],
			[{color: []}],
			['link']
		]
	};

	constructor(
		private configService: ConfigService,
		private blogService: BlogService,
		private dialogRef: MatDialogRef<ManagePanelComponent>,
		@Inject(MAT_DIALOG_DATA) public data,
		private snackBar: MatSnackBar) {

		this.filteredCategories = this.categoryControl.valueChanges.pipe(
			startWith(null, ''),
			map((category: any) => category ? this.filterCategories(category) : this.availableCategories.filter((x) => {
				return !this.categories.has(x);
			})));

	}

	ngOnInit(): void {
		this.config = this.configService.config;
		this.blogForm.patchValue({user_id: this.configService.user?.value?.id});

		if (this.data?.blog) {
			this.image = this.data.blog.banner;
			this.blogForm.patchValue(this.data.blog);
			this.categories = new Set(this.data.blog.categories);
			this.tags = new Set(this.data.blog.tags);

		}

		this.blogService.categoryListGet().toPromise().then(res => {
			for (const category of res) {
				category.category_id = category.id;
			}
			this.availableCategories = res;
		});

		this.blogForm.controls.title.valueChanges.subscribe(value => {
			if (!this.blogForm.controls.slug.dirty) {
				this.blogForm.controls.slug.patchValue(value.replace(/\s+/g, '-').toLowerCase());
			}
		});
	}

	public submit(): void {
		this.blogForm.markAllAsTouched();
		if (this.blogForm.valid) {
			if (this.blogForm.controls.published) {
				this.blogForm.controls.published_at.patchValue(new Date().getTime());
			} else {
				this.blogForm.controls.published_at.patchValue(null);
			}
			if (!this.data?.blog) {
				this.blogService.blogSave({
					...this.blogForm.value,
					tags: [...this.tags],
					categories: [...this.categories]
				}).pipe(
					tap(x => this.saving = true),
					finalize(() => this.saving = false)
				).subscribe((res: any) => {
					this.dialogRef.close({new: true, item: res});
				}, err => {
					this.snackBar.open(get(err, 'error.message', 'An error has occurred'), 'Error', {panelClass: 'error-snack'});
				});
			} else {
				this.blogService.blogUpdate({
					...this.blogForm.value,
					categories: [...this.categories],
					tags: [...this.tags],
				}).pipe(
					tap(() => this.saving = true),
					finalize(() => this.saving = false)
				).subscribe((res: any) => {
					this.dialogRef.close({updated: true, item: res});
				}, err => {
					this.snackBar.open(get(err, 'error.message', 'An error has occurred'), 'Error', {panelClass: 'error-snack'});
				});
			}
		} else {
			if (!this.blogForm.controls.user_id.value) {
				this.snackBar.open('Please Login', 'Error', {panelClass: 'error-snack'});
			} else {
				this.snackBar.open('Please Fix Form', 'Error', {panelClass: 'error-snack'});
			}
		}
	}

	delete() {
		this.blogService.blogDelete(this.data.blog.id)
			.pipe(
				tap(() => this.deleting = true),
				finalize(() => this.deleting = false)
			)
			.subscribe((res: any) => {
				this.dialogRef.close({deleted: true, item: {id: this.data.blog.id}});
			}, err => {
				this.snackBar.open(get(err, 'error.message', 'An error has occurred'), 'Error', {panelClass: 'error-snack'});
			});
	}

	addTag(event: MatChipInputEvent): void {
		const input = event.input;
		const value = event.value;

		if ((value || '').trim()) {
			this.tags.add(value.trim());
		}
		if (input) {
			input.value = '';
		}
	}

	removeTag(tag): void {
		this.tags.delete(tag);
	}


	addCategory(event: MatChipInputEvent): void {
		const input = event.input;
		const value = event.value;
		const category = this.availableCategories.find(x => x.category === value);

		if (value && category) {
			this.categories.add(category);
		}

		if (input) {
			input.value = '';
		}

		this.categoryControl.setValue(null);
	}

	removeCategory(category: string): void {
		this.categories.delete(category);
	}

	selectedCategory(event: MatAutocompleteSelectedEvent): void {
		const category = this.availableCategories.find(x => x.category === event.option.value.category);

		if (category) {
			this.categories.add(category);
		}
		this.categoryInput.nativeElement.value = '';
		this.categoryControl.setValue(null);
	}

	private filterCategories(value: any): any[] {
		const filterValue = typeof value === 'string' ?  value.toLowerCase() : value.category.toLowerCase();

		return this.availableCategories.filter(category => {
			return category.category.toLowerCase().indexOf(filterValue) === 0 && !this.categories.has(category);
		});
	}

	upload(event) {
		this.uploading = true;
		const file: File = event.target.files[0];
		const formData: FormData = new FormData();
		formData.append('content', file, file.name);
		this.blogService.blogBannerImageSave(formData)
			.pipe(
				finalize(() => this.uploading = false)
			)
			.subscribe(res => {
				this.blogForm.controls.banner.patchValue(res);
				const reader = new FileReader();
				reader.onload = () => this.image = reader.result;
				reader.readAsDataURL(file);
			}, error => this.snackBar.open('Image Upload', 'Failed', {panelClass: 'error-snack'}));
	}

	public deleteImage() {
		this.blogForm.controls.banner.patchValue(null);
	}
}
