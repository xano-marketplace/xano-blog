import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {XanoService} from './xano.service';
import {ApiService} from './api.service';

export interface XanoConfig {
	title: string;
	summary: string;
	editText: string;
	editLink: string;
	descriptionHtml: string;
	logoHtml: string;
	requiredApiPaths: string[];
}

@Injectable({
	providedIn: 'root'
})


export class ConfigService {
	public xanoApiUrl: BehaviorSubject<any> = new BehaviorSubject<any>(null);
	public user: BehaviorSubject<any> = new BehaviorSubject<any>(null);
	public authToken: BehaviorSubject<any> = new BehaviorSubject<any>(null);
	public config: XanoConfig = {
		title: 'Xano Blog Demo',
		summary: 'This extension provides a fully functioning demo to manage a blog.',
		editText: 'Get source code',
		editLink: 'https://gitlab.com/xano-marketplace/xano-blog',
		descriptionHtml: `
                <h2>Description</h2>
                <p>
                	This demo illustrates the implementation and functionality of the Xano Blog Template on your front end. This demo 
                	allows for multiple user accounts that can all have their personal blogs that is shared between all users of the app.
                </p>
                <br /> 
                <p>This demo consists of following components:</p>
                <h2>Home View</h2>
                <p>
                	In this view you will be able to see published blogs that are organized by category (newest 3 in each category).
                	 Additionally, there is a search capabilities that you can use to filter all published blogs.
				</p>
				<h2>Blog View</h2>
				<p>
					Here you will be able to read the blog contents, as well as comment either anonymously or, if logged-in, with by your user.
				</p>
				<h2>Search</h2>
                <p>A search box is present to allow you to search the title, description, and content, or tags of blogs.</p>
                <h2>Personal View</h2>
                <p>
                	Once authenticated you can view or search all your published and draft blogs. As well as create new blogs or edit
                	 and delete existing blogs. The blog uses a rich text editor for a more applicable demonstration. You will be able to
                	comment on both your blog and other users blogs that have been published. Additionally you can moderate your own 
                	comments on others blogs or all comments that are posted to your blog.
                 </p>
                <h2>Manage View</h2>
                <p>This is a side panel that lets you sign-up for the blog service, sign in, and do any CRUD operations on your blogs</p>
                `,
		logoHtml: '',
		requiredApiPaths: [
			'/blog',
			'/blog/{slug}',
			'/blog/{blog_id}',
			'/categories',
			'/me/blog',
			'/category',
			'/auth/login',
			'/auth/signup',
			'/user',
			'/comments'
		]
	};

	constructor(private http: HttpClient, private xanoService: XanoService, private apiService: ApiService) {
	}

	public isConfigured(): Observable<any> {
		return this.xanoApiUrl.asObservable();
	}

	public isLoggedIn(): Observable<any> {
		return this.authToken.asObservable();
	}

	public configGet(apiUrl): Observable<any> {
		return this.apiService.get({
			endpoint: this.xanoService.getApiSpecUrl(apiUrl),
			headers: {
				Accept: 'text/yaml'
			},
			responseType: 'text',
		});
	}
}
