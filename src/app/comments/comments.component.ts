import {Component, Input, OnInit} from '@angular/core';
import {FormControl, Validators} from '@angular/forms';
import {BlogService} from '../blog.service';
import {ConfigService} from '../config.service';
import {finalize} from 'rxjs/operators';

@Component({
	selector: 'app-comments',
	templateUrl: './comments.component.html',
	styleUrls: ['./comments.component.scss']
})
export class CommentsComponent implements OnInit {

	public commentInput: FormControl = new FormControl('', Validators.required);
	public comments = [];
	public user: any;
	@Input('blogId') blogId: number;
	@Input('blogOwnerId') blogOwnerId: number;

	constructor(
		private blogService: BlogService,
		private configService: ConfigService
	) {
	}

	ngOnInit(): void {
		this.configService.user.asObservable().subscribe(res => this.user = res);
		setTimeout(() => {
			this.blogService.blogCommentGet(this.blogId).subscribe(res => this.comments = res);
		}, 1000);
	}

	public postComment() {
		if (this.commentInput.value) {
			this.blogService.blogCommentSave({
				comment: this.commentInput.value,
				blog_id: this.blogId,
				user_id: this.user?.id ? this.user.id : 0
			}).toPromise().then(res => {
				console.log(res);
				this.comments.unshift(res);
				this.commentInput.patchValue('');
			});
		}
	}

	public allowDelete(comment): boolean {
		return this.user?.id && (comment.user_id === this.user?.id || this.blogOwnerId === this.user?.id);
	}

	public deleteComment(comment) {
		this.blogService.blogCommentDelete(comment.id).pipe(
			finalize(() => this.comments = this.comments.filter(x => x.id !== comment.id))
		).subscribe();
	}

}
