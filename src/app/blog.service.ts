import {Injectable} from '@angular/core';
import {ApiService} from './api.service';
import {ConfigService} from './config.service';
import {Observable} from 'rxjs';

@Injectable({
	providedIn: 'root'
})
export class BlogService {

	constructor(private apiService: ApiService, private configService: ConfigService) {
	}

	public categoryGet(): Observable<any> {
		return this.apiService.get({
			endpoint: `${this.configService.xanoApiUrl.value}/category`,
		});
	}

	public categoryListGet(): Observable<any> {
		return this.apiService.get({
			endpoint: `${this.configService.xanoApiUrl.value}/categories`,
		});
	}

	public blogsGet(search, categoryId): Observable<any> {
		const params = { search };
		if (categoryId) {
			params['category_id'] = categoryId;
		}
		return this.apiService.get({
			endpoint: `${this.configService.xanoApiUrl.value}/blog`,
			params: params
		});
	}

	public blogSave(blog): Observable<any> {
		return this.apiService.post({
			endpoint: `${this.configService.xanoApiUrl.value}/blog`,
			headers: {Authorization: `Bearer ${this.configService.authToken.value}`},
			params: blog,
		});
	}


	public blogGet(slug): Observable<any> {
		return this.apiService.get({
			endpoint: `${this.configService.xanoApiUrl.value}/blog/${slug}`,
		});
	}

	public blogDelete(blogId): Observable<any> {
		return this.apiService.delete({
			endpoint: `${this.configService.xanoApiUrl.value}/blog/${blogId}`,
			headers: {Authorization: `Bearer ${this.configService.authToken.value}`},
		});
	}

	public blogUpdate(blog): Observable<any> {
		return this.apiService.post({
			endpoint: `${this.configService.xanoApiUrl.value}/blog/${blog.id}`,
			headers: {Authorization: `Bearer ${this.configService.authToken.value}`},
			params: blog
		});
	}

	public blogCommentSave(comment): Observable<any> {
		return this.apiService.post({
			endpoint: `${this.configService.xanoApiUrl.value}/comments`,
			params: comment
		});
	}

	public blogCommentGet(blogId): Observable<any> {
		return this.apiService.get({
			endpoint: `${this.configService.xanoApiUrl.value}/comments/blog/${blogId}`,
		});
	}

	public blogCommentDelete(commentId): Observable<any> {
		return this.apiService.delete({
			endpoint: `${this.configService.xanoApiUrl.value}/comments/${commentId}`,
			headers: {Authorization: `Bearer ${this.configService.authToken.value}`},
		});
	}

	public blogBannerImageSave(image): Observable<any> {
		return this.apiService.post({
			endpoint: `${this.configService.xanoApiUrl.value}/upload/image`,
			params: image
		});
	}
}
