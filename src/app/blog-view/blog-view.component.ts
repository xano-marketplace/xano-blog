import {Component, EventEmitter, OnInit} from '@angular/core';
import {BlogService} from '../blog.service';
import {ActivatedRoute, Router} from '@angular/router';
import {concatMap, finalize, tap} from 'rxjs/operators';
import {ConfigService} from '../config.service';

@Component({
	selector: 'app-blog-view',
	templateUrl: './blog-view.component.html',
	styleUrls: ['./blog-view.component.scss']
})
export class BlogViewComponent implements OnInit {
	public blog: any;
	public loggedIn: boolean = false;
	public loading: boolean = true;

	constructor(
		private blogService: BlogService,
		private route: ActivatedRoute,
		private router: Router,
		private configService: ConfigService,
	) {
	}

	ngOnInit(): void {
		this.configService.isLoggedIn().subscribe(res => {
			this.loggedIn = res;
		});
		this.configService.isLoggedIn().subscribe(loggedIn => this.loggedIn = loggedIn);
		this.route.params.pipe(concatMap(params => this.blogService.blogGet(params.slug)
			.pipe(
				tap(() => this.loading = true),
				finalize(() => this.loading = false)
			))).subscribe(blog => {
			this.blog = blog;
		});
	}

	public getBlog(slug) {
		this.blogService.blogGet(slug).toPromise().then(blog => {
			this.blog = blog;
		});
	}
}
